import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Button, Autocomplete, TextInput, Table, Pane, Dialog, Text} from "evergreen-ui";
import firebase from 'firebase'
import './style.css'


var config = {
    apiKey: "AIzaSyCQ737rtilJ3fJmz-L-ulL_-PzNjv02heg",
    authDomain: "my-second-project-201c8.firebaseapp.com",
    databaseURL: "https://my-second-project-201c8.firebaseio.com",
    projectId: "my-second-project-201c8",
    storageBucket: "my-second-project-201c8.appspot.com",
    messagingSenderId: "1022485828054"
  };
  firebase.initializeApp(config);

let Context = React.createContext()

let Title = styled.h1`
    @import url('https://fonts.googleapis.com/css?family=Pacifico');
    font-family: 'Pacifico', cursive;
    color : #c94c4c ;

`
let HistoryTitle = styled.h3`
    @import url('https://fonts.googleapis.com/css?family=Assistant');
    font-family: 'Assistant', sans-serif;
    color : #c94c4c ;

`


class Druginput extends Component {
    render(){
        return(
            <Context.Consumer>
                {
                    (ctx)=>{
                        return(
                            <Autocomplete
                                title="Drugs"
                                onChange={changedItem => ctx.actions.addToList(changedItem)}
                                items={ctx.state.drugsList}
                                >
                                {(props) => {
                                    const { getInputProps, getRef, inputValue } = props
                                    return (
                                    <TextInput
                                        placeholder="Please Choose a Drug"
                                        value={inputValue}
                                        innerRef={getRef}
                                        {...getInputProps()}
                                    />
                                    )
                                }}
                            </Autocomplete>
                        )
                    }
                }
            </Context.Consumer>
        )
    }
}

class Druglist extends Component {
    render(){
        return(
            <Context.Consumer>
                {
                    (ctx)=>{
                        return(
                            <Table className="Table">
                                <Table.Head>
                                    <Table.TextHeaderCell>Drugs</Table.TextHeaderCell>
                                </Table.Head>
                                <Table.Body>
                                    {ctx.state.chosenDrugs.map(drug => (
                                    <Table.Row >
                                        <Table.TextCell>{drug}</Table.TextCell>
                                    </Table.Row>
                                    ))}
                                </Table.Body>
                            </Table>
                        )
                    }
                }
            </Context.Consumer>
        )
    }
}

class PatientInfo extends Component {
    constructor() {
        super()
        this.state= {
            isShown : false
        }
    }
    render(){
        return(
            <Context.Consumer>
                {
                    (ctx)=>{
                        return(
                            
                                    <Pane>
                                    <Dialog
                                        isShown={this.state.isShown}
                                        title="Patient Information"
                                        onCloseComplete={() =>{
                                            this.setState({ isShown: false })
                                            
                                            firebase.firestore().collection('Prescriptions').add({
                                                patientName : ctx.state.patientName,
                                                patientAge : ctx.state.patientAge,
                                                Prescription : ctx.state.chosenDrugs
                                            })
                                        }}
                                        confirmLabel="Save And Export PDF"
                                    >
                                        Patient Name  <br/>
                                        <TextInput name="text-input-name" placeholder="Patient Name" value={ctx.state.patientName} onChange={(event)=>{ctx.actions.updateName(event.target.value)}} />
                                        <br/> <br/> 
                                        Patient Age  <br/>
                                        <TextInput name="text-input-name" placeholder="Patient Age" value={ctx.state.patientAge} onChange={(event)=>{ctx.actions.updateAge(event.target.value)}} />
                                        <br/> <br/> 
                                        Patient Prescription
                                        <ul>
                                            {
                                                ctx.state.chosenDrugs.map((drug, i)=>{
                                                    return <li key={i}> {drug} </li>
                                                   
                                                })
                                            }
                                        </ul>
                                    </Dialog>

                                    <Button onClick={() => this.setState({ isShown: true })} >Enter patient Info</Button>
                                    </Pane>
                               
                        )
                    }
                }
            </Context.Consumer>
        )
    }
}

// class History extends Component {
//     render(){
//         return(
//             <Context.Consumer>
//                 {
//                     (ctx)=>{
//                         ctx.state.Presc.map((patient)=>{
//                             return (<Pane
//                                 elevation={1}
//                                 float="left"
//                                 width={200}
//                                 height={120}
//                                 margin={24}
//                                 display="flex"
//                                 justifyContent="center"
//                                 alignItems="center"
//                                 flexDirection="column"
//                               >
//                                 <Text>{patient.patientName}</Text>
//                                 <Text size={300}>{patient.patientAge}</Text>
//                               </Pane>)   
//                         })
//                     }
//                 }
//             </Context.Consumer>
//         )
//     }
// }


class App extends Component {
    constructor(){
      super()
      this.state = {
        drugsList: [],
        chosenDrugs :[],
        patientName :"",
        patientAge :""
        // Presc :[]
      }


    firebase.firestore().collection("drugs")
    .orderBy("drug_id", "asc").onSnapshot((snapshot)=>{

      let drugs = []
      snapshot.forEach((doc)=>{
        drugs.push(doc.data().drug)
      })

      this.setState({
        drugsList: drugs
      })
    })


    // let coll =[]
    // firebase.firestore().collection('Prescriptions').onSnapshot((snapshot)=>{
    //     snapshot.forEach((doc)=>{
    //         coll.push(doc.data())
    //     })
    //     this.setState({
    //         Presc: coll
    //       })
    // })

    }
  
    render(){
      return (
        <Context.Provider value={{ 
          state: this.state,
          actions : {
              addToList : (chosenDrug)=>{
                  let chosenList = this.state.chosenDrugs
                  chosenList.push(chosenDrug)

                  this.setState({
                      chosenDrugs : chosenList
                  })
              },
              updateInput: (value)=>{
                this.setState({
                  messageContent: value
                })
              },
              updateName: (value)=>{
                this.setState({
                  patientName: value
                })
              },
              updateAge: (value)=>{
                this.setState({
                  patientAge: value
                })
              }
          }

          }}>
          <Title>My Patients</Title>
            <Druginput />
            <Druglist />
            <PatientInfo />
            <HistoryTitle>Patients History</HistoryTitle>
        </Context.Provider>
      )
    }
  }



ReactDOM.render(<App />, document.getElementById('root'))